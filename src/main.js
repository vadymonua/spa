import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import VueAxios from "vue-axios";

import axios from "./axios/";
import router from "@/router/index";

/*import axiosUser from './axios/users'
axiosUser(axios)*/


import App from './App.vue'


import 'bootstrap/scss/bootstrap.scss'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/style.scss'


Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueAxios, axios)

Vue.config.productionTip = false

new Vue({
  router: router,
  render: h => h(App),
}).$mount('#app')
