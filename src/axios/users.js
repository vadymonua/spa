export const getUsers = (axios, url = '/users') => {
    const users = JSON.parse(localStorage.getItem('users')) || []
    if (users.length)
        return new Promise(resolve => resolve(users))

    return axios.get(url)
        .then(({data: users}) => {
            localStorage.setItem('users', JSON.stringify(users))
            return users
        })
}


export const updateUser = (axios, url, data) => {
    return new Promise((resolve, reject) => {
            axios.patch(url, data)
                .then(({data: user}) => {
                    const users = JSON.parse(localStorage.getItem('users')) || []
                    if (users.length) {
                        const oldUser = users.find(u => u.id === user.id)
                        const oldUserIndex = users.indexOf(oldUser)
                        users.splice(oldUserIndex, 1, user)
                    }
                    localStorage.setItem('users', JSON.stringify(users))
                    resolve(users)
                })
                .catch(error => reject(error))
        }
    )
}


