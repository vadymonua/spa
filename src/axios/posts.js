export const getPostsByUserId = (axios,userId,params,url='/posts', )=>{
    return axios.get(url,{params:{...params, userId}})
        .then(({data:posts})=>posts)
}