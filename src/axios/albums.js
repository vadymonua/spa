export const getAlbumsByUserId = (axios,userId,params,url='/albums', )=>{
    return axios.get(url,{params:{...params, userId}})
        .then(({data:albums})=>albums)
}