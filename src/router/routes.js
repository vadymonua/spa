import Home from "@/components/pages/Home";
import User from "@/components/pages/User";
import AboutUs from "@/components/pages/AboutUs";
import Contacts from "@/components/pages/Contacts";

export default [
    {
        path: '/',
        redirect: '/users'

    },
    {
        path: '/users',
        name: 'users',
        component: Home
    },
    {
        path: '/user:id',
        name: 'user',
        component: User
    },
    {
        path: '/about-us',
        name: 'about-us',
        component: AboutUs
    },
    {
        path: '/contacts',
        name: 'contacts',
        component: Contacts
    }

]