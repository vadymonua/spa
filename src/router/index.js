import Vue from "vue";
import VueRouter from "vue-router";
import routes from "./routes";

Vue.use(VueRouter)


const router = new VueRouter({
    mode:'history',
    routes
})
//TODO hooks and other settings

export default router